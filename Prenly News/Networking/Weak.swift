
import Foundation

class Weak<Object: AnyObject> {
    private(set) weak var object: Object?
    
    init(object: Object) {
        self.object = object
    }
}
