//
//  NewsSearchRequest.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import Foundation

//MARK: Request Creation
class NewsSearchRequest: Request, Codable {
    
    var keyWord: String
    
    var path: String {
        return "everything?q=\(keyWord)&from=2021-09-10&sortBy=popularity&apiKey=\(AppConstants.apiKey)"
    }
    
    var method: RequestMethod {
        return .get
    }
    
    init(keyWord: String) {
        self.keyWord = keyWord
    }
}

//MARK: Response Models

struct NewsSearchResponse: Codable {
    let status: String?
    let totalResults: Int?
    let articles: [Article]?
}

struct Article: Codable {
    let source: Source?
    let author: String?
    let title: String?
    let description: String?
    let url: String?
    let urlToImage: String?
    let publishedAt: String?
    let content: String?
}

struct Source: Codable {
    let id: String?
    let name: String?
}
