//
//  SplashViewController.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import UIKit

class SplashViewController: BaseViewController {

    //MARK: life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    //MARK: Setup configuration
    func configure() {
        router.showMainScreen()
    }

}
