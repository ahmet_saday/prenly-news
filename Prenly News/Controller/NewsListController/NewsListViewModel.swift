//
//  NewsListViewModel.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import Foundation

class NewsListViewModel: BaseViewModel {
    
    //MARK: variables and IBOutlets
    var articles = Observable<[Article]>()
    
    //MARK: life cycle methods
    override init() {
        super.init()
        articles.value = []
    }
    
    //MARK: Methods
    // the method fetches news by keyword and set the articles array
    func getNewsByKeyWord(keyWord: String) {
        let spacelessKeyWord = keyWord.replacingOccurrences(of: " ", with: "+")
        self.loaderState.value = .show
        
        let request = NewsSearchRequest(keyWord: spacelessKeyWord)
        RestClient.default.makeRequest(request: request) { (response: NewsSearchResponse?, error: RestClient.Error?) in
            self.loaderState.value = .dismiss
            guard let _response = response else {
                self.errorState.value = .showWith(title: "", message: "Please search another news !!")
                return
            }
            
            if let _ = error {
                self.errorState.value = ErrorState.showWith(title: "", message: error.debugDescription)
                return
            }
            
            if let _articles = _response.articles {
                self.articles.value = _articles
            }
        }
    }
}
