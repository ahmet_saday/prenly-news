//
//  NewsListCell.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import UIKit

class NewsListCell: UITableViewCell {

    //MARK: variables and IBOutlets
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: Methods
    // the method arranges the cell's values
    func arrange(news: Article) {
        self.newsTitleLabel.text = news.title
        self.newsImageView.download(url: news.urlToImage)
    }
    
}
