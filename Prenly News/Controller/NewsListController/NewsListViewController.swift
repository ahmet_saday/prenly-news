//
//  NewsListViewController.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import UIKit

class NewsListViewController: BaseViewController {
    
    //MARK: variables and IBOutlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var newsTableview: UITableView!
    @IBOutlet weak var emptyView: UIView!
    
    var viewModel: NewsListViewModel!

    //MARK: life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()

        configure()
    }
    
    //MARK: Setup configuration
    func configure() {
        self.title = "News List"
        // set view model
        self.setViewModel(viewModel: viewModel)
        
        // tableview configuration
        newsTableview.delegate = self
        newsTableview.dataSource = self
        newsTableview.register(UINib(nibName: "NewsListCell", bundle: nil), forCellReuseIdentifier: "NewsListCell")
        newsTableview.separatorStyle = .none
        
        // text field configuration
        self.searchTextField.delegate = self
        
        // set a subscribe to viewmodels articles. It will be triggered when articles change.
        viewModel.articles.subscribe { (article) in
            self.emptyView.isHidden = true
            self.newsTableview.reloadData()
        }   
    }
}

//MARK: TableView Delegate and DataSource
extension NewsListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.articles.value?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = newsTableview.dequeueReusableCell(withIdentifier: "NewsListCell", for: indexPath) as? NewsListCell {
            if let news = viewModel.articles.value?[indexPath.row] {
                cell.arrange(news: news)
                return cell
            }
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let _news = viewModel.articles.value?[indexPath.row] {
            self.router.showNewsDetail(news: _news)
        }
    }
    
    
}

//MARK: TextField Delegate
extension NewsListViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        mainThread {
            self.viewModel.getNewsByKeyWord(keyWord: self.searchTextField.text ?? "")
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
