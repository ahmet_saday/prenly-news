//
//  NewsDetailViewController.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import UIKit
import Kingfisher

class NewsDetailViewController: BaseViewController {
    
    //MARK: variables and IBOutlet
    @IBOutlet weak var newsImageView: UIImageView!
    @IBOutlet weak var newsTitleLabel: UILabel!
    @IBOutlet weak var newsContentLabel: UILabel!
    
    @IBOutlet weak var newsLinkTextView: UITextView!
    
    var viewModel: NewsDetailViewModel!
    
    //MARK: life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configure()
    }

    //MARK: Setup configuration
    func configure() {
        self.navigationItem.title = "News Detail"
        
        // set view model
        self.setViewModel(viewModel: viewModel)
        // set ui items
        self.newsImageView.download(url: viewModel.news.urlToImage)
        self.newsTitleLabel.text = viewModel.news.title
        self.newsContentLabel.text = self.getNewsContext()
        
        let attributedString = NSMutableAttributedString(string: viewModel.news.url ?? "")
        self.newsLinkTextView.attributedText = attributedString
        
    }

    //MARK: Methods
    // getNewsContext arranges the value of the news content
    func getNewsContext() -> String {
        var content = ""
        content = "\(viewModel.news.description ?? "") \("\n\n") \(viewModel.news.content ?? "")  \("\n\n") \(viewModel.news.source?.name ?? "") - \(viewModel.news.author ?? "")  \("\n") \(viewModel.news.publishedAt?.getDate() ?? "")"

        return content
    }

}

