//
//  NewsDetailViewModel.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import Foundation

class NewsDetailViewModel: BaseViewModel {
    //MARK: variables and constants
    let news: Article!
    
    //MARK: initial methods
    init(news:Article) {
        self.news = news
        super.init()
        
    }
}
