//
//  BaseViewController.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import UIKit
import Reachability

class BaseViewController: UIViewController {
    
    var router = BaseRouter()
    private var viewModel: BaseViewModel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureRouter()
        // subscribe to ReachabilityManager
        ReachabilityManager.shared.setSubscribe(controller: self)
        
    }
    
    // router configurations
    private func configureRouter() {
        
        router.push.subscribe { (controller) in
            
            guard let _controller = controller else {
                return
            }
            self.navigationController?.pushViewController(_controller, animated: true)
            
        }
        
        router.present.subscribe { (controller) in
            
            guard let _controller = controller else {
                return
            }
            self.present(_controller, animated: true, completion: nil)
        }
        
    }
    
    // default viewcontroller behaviors
    func setViewModel(viewModel: BaseViewModel) {
        self.viewModel = viewModel
       
        // default loader cases
        self.viewModel.loaderState.subscribe { (loader) in
            guard let _loader = loader else {
                return
            }
            switch _loader {
            case .show:
                self.showLoader()
                break
            case .dismiss:
                self.dismissLoader()
                break
            }
            
        }
        
        // default error cases
        self.viewModel.errorState.subscribe { (error) in
            guard let _error = error else {
                return
            }
            
            switch _error {
            case .showWith(let title, let message):
                self.showMessageWith(title: title, message: message)
                break
            case .logout:
                //TODO --
                break
            case .networkError:
                self.showMessageWith(title: "", message: "You don't have any network connection. Please check your connection.")
            case .none:
                break
            }
        }
        
    }
    
    // Action method for connection problems
    func reachabilityChangedStatus(reachibility: Reachability) {
        //If you want subscribe reachability and check connection
    }
}

//MARK: App Loader Methods
extension BaseViewController {
    
    func showMessageWith(title: String, message: String) {
        mainThread {
            
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)

        }
    }
    
    func showLoader() {
        Loader.show()
    }
    
    func dismissLoader() {
        Loader.dismiss()
    }

}

//MARK: Screen creation methods
extension BaseViewController {
    
    @objc class var storyboardName: String {
        return String(describing: self)
    }
    
    @objc class func createFromStoryboard() -> Self {
        let vc = createFromStoryboard(named: storyboardName, type: self)
        if let vc = vc as? UIViewController {
            vc.modalPresentationStyle = .fullScreen
        }
        return vc
    }
    
    static func createFromStoryboard<T: BaseViewController>(named storyboardName: String?, type: T.Type) -> T {
        let vc = UIStoryboard(name: storyboardName ?? self.storyboardName, bundle: Bundle.main).instantiateInitialViewController() as! T
        if let vc = vc as? UIViewController {
            vc.modalPresentationStyle = .fullScreen
        }
        return vc
    }
    
    
}
