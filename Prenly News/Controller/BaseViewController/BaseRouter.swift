//
//  BaseRouter.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import UIKit

class BaseRouter {
    
    //MARK: overserver variables
     var push = Observable<UIViewController>()
     var present = Observable<UIViewController>()

    //MARK: screen creation methods
    func showNewsDetail(news: Article) {
        let vc = NewsDetailViewController.createFromStoryboard()
        vc.viewModel = NewsDetailViewModel(news: news)
        self.push.value = vc
    }
    
    func showMainScreen() {
        let vc = NewsListViewController.createFromStoryboard()
        vc.viewModel = NewsListViewModel()
        let navigation = UINavigationController(rootViewController: vc)
        UIApplication.shared.windows.first?.rootViewController = navigation
    }
    
}
