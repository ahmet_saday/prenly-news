//
//  BaseViewModel.swift
//  Prenly News
//
//  Created by Saday on 10.09.2021.
//

import Foundation

class BaseViewModel {
    //MARK: Observable variables
    var errorState = Observable<ErrorState>()
    var loaderState = Observable<LoaderState>()
}

//MARK: Enums
enum ErrorState {
    case showWith(title: String, message: String)
    case networkError
    case logout
    case none
}

enum LoaderState {
    case show
    case dismiss
}
