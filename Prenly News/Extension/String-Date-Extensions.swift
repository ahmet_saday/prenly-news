//
//  String-Date-Extensions.swift
//  Prenly News
//
//  Created by Saday on 11.09.2021.
//

import Foundation

extension String {
    func getDate() -> String? {
        let dateFormatter = DateFormatter()
        let newDateFormatter = DateFormatter()
        newDateFormatter.dateFormat = "yyyy-MM-dd"
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        if let date = dateFormatter.date(from: self) {
            return newDateFormatter.string(from: date)
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        if let date = dateFormatter.date(from: self) {
            return newDateFormatter.string(from: date)
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateFormatter.date(from: self) {
            return newDateFormatter.string(from: date)
        }
        
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        if let date = dateFormatter.date(from: self) {
            return newDateFormatter.string(from: date)
        }
        
        
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm:ss"
        if let date = dateFormatter.date(from: self) {
            return newDateFormatter.string(from: date)
        }
        
        dateFormatter.dateFormat = "dd.MM.yyyy"
        if let date = dateFormatter.date(from: self){
            return newDateFormatter.string(from: date)
        }
        
        return nil
    }
}
